import * as React from "react";
import { Todo } from "./Interfaces";
import * as cx from "classnames";

interface Props {
  todo: Todo;
  onToggle: (todo: Todo) => void;
  onRemove: (todo: Todo) => void;
  onEdit: (todo: Todo, title: string) => void;
}

interface State {
  editing: boolean;
  newTitle: string;
}

export default class TodoItem extends React.PureComponent<Props, State> {
  /*
   * Sur ReactJS, l'héritage "React.PureComponent" permet d'appellé le render du composant seulement si la condition est rempli
   * dans notre cas, il est important de vérifier si la todo actuelle est différente de la todo passé en props
   * car il est inutile d'appellé le render de ce composant TodoList lorsqu'on renseigne le titre d'une nouvelle todo, par exemple
   * Sur PreactJS, cette classe héritage n'existe pas, il faut utiliser la fonction "shouldComponentUpdate"
   * Exemple:
      shouldComponentUpdate({ todo }: Props, state: State) {
        return todo !== this.props.todo;
      }
  */

  constructor(props: Props) {
    super(props);
    this.state = {
      editing: false,
      newTitle: ""
    };
  }

  render() {
    let { todo } = this.props;
    let { newTitle, editing } = this.state;
    return (
      <li className={cx({ completed: todo.completed, editing: editing })}>
        <div className="view">
          <input className="toggle" type="checkbox" onChange={this.onToggleEvent} checked={todo.completed} />
          <label onDoubleClick={this.startEditing}>{todo.title}</label>
          <button className="destroy" onClick={this.onRemoveEvent} ></button>
        </div>
        <input className="edit"
          type="text"
          value={newTitle}
          onBlur={this.handleCancelEditing}
          onKeyDown={this.handleKeyDown}
          onInput={this.handleInput} />
      </li>
    );
  }

  onToggleEvent = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.props.onToggle(this.props.todo);
  }
  onRemoveEvent = () => {
    this.props.onRemove(this.props.todo);
  }

  startEditing = (e: React.MouseEvent<HTMLLabelElement>) => {
    this.setState({ editing: true, newTitle: this.props.todo.title });
  }
  handleSubmit = () => {
    this.props.onEdit(this.props.todo, this.state.newTitle);
    this.setState({ editing: false, newTitle: "" });
  }
  handleCancelEditing = () => {
    this.setState({ editing: false, newTitle: "" });
  }
  handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    switch (e.key) {
      case "Enter":
        this.handleSubmit();
        break;

      case "Escape":
        this.handleCancelEditing();
        break;

      default:
        break;
    }
  }
  handleInput = (e: React.FormEvent<HTMLInputElement>) => {
    let value = e.currentTarget.value;
    this.setState({ newTitle: value });
  }
}
