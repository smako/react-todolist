import * as cx from "classnames";
import * as React from "react";
import { Todo } from "./Interfaces";
import TodoItem from "./TodoItem";
import { TodoStore } from "./TodoStore";

declare type FilterOptions = "all" | "active" | "completed";

const FILTERS = {
  completed: (todo: Todo) => todo.completed,
  active: (todo: Todo) => !todo.completed,
  all: (todo: Todo) => true
};

interface TodoListProps { }
interface TodoListState {
  todos: Todo[];
  newTodoTitle: string;
  filter: FilterOptions;
}

export default class TodoList extends React.Component<TodoListProps, TodoListState> {
  private store: TodoStore = new TodoStore();
  private toggleTodo: (todo: Todo) => void;
  private removeTodo: (todo: Todo) => void;
  private clearCompleted: () => void;
  private editTodo: (todo: Todo, title: string) => void;

  constructor(props: TodoListProps) {
    super(props);
    this.state = {
      todos: [],
      newTodoTitle: "",
      filter: "all"
    };
    this.store.onChange((store) => this.setState({ todos: store.todos }));
    this.toggleTodo = this.store.toggleTodo.bind(this.store);
    this.removeTodo = this.store.removeTodo.bind(this.store);
    this.clearCompleted = this.store.clearCompleted.bind(this.store);
    this.editTodo = this.store.editTodo.bind(this.store);
  }

  get remainingCount(): Number {
    return this.state.todos.reduce((count, todo) => !todo.completed ? count + 1 : count, 0);
  }
  get completedCount(): Number {
    return this.state.todos.reduce((count, todo) => todo.completed ? count + 1 : count, 0);
  }

  render() {
    let { todos, newTodoTitle, filter } = this.state;
    let todosFiltered = todos.filter(FILTERS[filter]);
    let remainingCount = this.remainingCount;
    let completedCount = this.completedCount;
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" placeholder="What needs to be done?"
            value={newTodoTitle}
            onInput={this.updateNewTodo}
            onKeyPress={this.submitNewTodo} />
        </header>
        <section className="main" data-children-count="1">
          {todos.length > 0 &&
            <input id="toggle-all" className="toggle-all" type="checkbox" checked={remainingCount === 0}
              onChange={this.toggleAllTodo} />
          }
          <label htmlFor="toggle-all">Mark all as complete</label>
          <ul className="todo-list">
            {todosFiltered.map(
              (todo) => {
                return (
                  <TodoItem
                    todo={todo}
                    key={todo.id}
                    onToggle={this.toggleTodo}
                    onRemove={this.removeTodo}
                    onEdit={this.editTodo}
                  />
                );
              }
            )}

          </ul>
        </section>
        <footer className="footer">
          {remainingCount > 0 && <span className="todo-count"><strong>{remainingCount}</strong> item{remainingCount > 1 && "s"} left</span>}
          <ul className="filters">
            <li>
              <a href="#/" className={cx({ selected: filter === "all" })} onClick={this.setFilter("all")}>All</a>
            </li>
            <li>
              <a href="#/active" className={cx({ selected: filter === "active" })} onClick={this.setFilter("active")}>Active</a>
            </li>
            <li>
              <a href="#/completed" className={cx({ selected: filter === "completed" })} onClick={this.setFilter("completed")}>Completed</a>
            </li>
          </ul>
          {completedCount > 0 && <button className="clear-completed" onClick={this.clearCompleted}>Clear completed</button>}
        </footer>
      </section >
    );
  }

  updateNewTodo = (e: React.FormEvent<HTMLInputElement>) => {
    let value = e.currentTarget.value;
    this.setState({ newTodoTitle: value });
  }

  submitNewTodo = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === "Enter") {
      this.store.addTodo(this.state.newTodoTitle);
      this.setState({ newTodoTitle: "" });
    }
  }

  toggleAllTodo = (e: React.FormEvent<HTMLInputElement>) => {
    this.store.toggleAll(this.remainingCount > 0);
  }

  setFilter = (filter: FilterOptions) => {
    return (e: React.MouseEvent<HTMLElement>) => {
      this.setState({ filter });
    };
  }
}
