import { Todo } from "./Interfaces";

declare type ChangeCallback = (store: TodoStore) => void;

export class TodoStore {
  private static _i: number = 0;
  private callbacks: ChangeCallback[] = [];

  public todos: Todo[] = [];

  static increment() {
    return this._i++;
  }

  inform() {
    this.callbacks.forEach(callback => callback(this));
  }

  onChange(callback: ChangeCallback) {
    this.callbacks.push(callback);
  }

  addTodo(title: string): void {
    /**
     * NOTE: Attention, ne pas faire de `push` car cela effectue une "mutation" dans le store.
     * ReactJS ne se comporte pas correctement avec les mutations
     */
    this.todos = this.todos.concat([{
      id: TodoStore.increment(),
      title: title,
      completed: false
    }]);
    this.inform();
  }

  removeTodo(todo: Todo): void {
    this.todos = this.todos.filter((el) => el !== todo);
    this.inform();
  }

  toggleTodo(todo: Todo): void {
    this.todos = this.todos.map((el) => (el === todo) ? { ...el, completed: !el.completed } : el);
    this.inform();
  }

  toggleAll(completed: boolean = true): void {
    this.todos = this.todos.map((el) => (el.completed !== completed) ? { ...el, completed } : el);
    this.inform();
  }

  editTodo(todo: Todo, title: string): void {
    this.todos = this.todos.map((el) => (el === todo) ? { ...el, title } : el);
    this.inform();
  }

  clearCompleted(): void {
    this.todos = this.todos.filter((el) => el.completed !== true);
    this.inform();
  }
}
